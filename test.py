from time import sleep
import unittest
from fake_useragent import UserAgent
from selenium.webdriver.common.by import By
from selenium import webdriver
from selenium.webdriver.chrome.options import Options


class TestKinopoisk(unittest.TestCase):

    def setUp(self):
        ops = Options()
        ops.add_argument("--no-sandbox")
        ops.add_argument("--disable-dev-shm-usage")
        # ops.add_argument(UserAgent().random)

        self.test_url1 = "https://www.kinopoisk.ru"
        self.test_url2 = "https://www.kinopoisk.ru/film/435/"
        self.test_url3 = "https://www.kinopoisk.ru/lists/categories/movies/1/"

        self.driver = webdriver.Remote(
            command_executor="http://selenium__standalone-chrome:4444/wd/hub",
            options=ops
        )

        # self.driver = webdriver.Firefox(executable_path="/usr/local/bin/geckodriver", options=ops)

    # test search of movie
    # fully automated
    def test_search_movie(self):
        self.driver.get(self.test_url1)

        input = self.driver.find_element(by=By.NAME, value="kp_query")
        input.send_keys("Зеленая миля")

        sleep(5)

        search_btn = self.driver.find_element(by=By.CLASS_NAME, value="styles_iconActive__dJx1_")
        search_btn.click()

        sleep(6)

        movie_title = self.driver.find_element(by=By.LINK_TEXT, value="Зеленая миля")

        assert movie_title.text.__contains__("Зеленая миля")

    # user can check top 250 movies of kinopoisk
    # was run locally
    # def test_top_250(self):
    #     self.driver.get(self.test_url3)
    #
    #     sleep(10)
    #
    #     top250_link = self.driver.find_element(by=By.CLASS_NAME, value="styles_root__c9qje")
    #     top250_link.click()
    #
    #     sleep(15)
    #
    #     # check first movie
    #     first_movie = self.driver.find_element(by=By.CLASS_NAME, value="styles_root__ti07r")
    #
    #     assert first_movie is not None

    # anon user can not rate
    # was run locally
    # def test_anon_can_not_rate(self):
    #
    #     self.driver.get(self.test_url2)
    #
    #     sleep(10)
    #
    #     rate_ten = self.driver.find_element(by=By.CLASS_NAME, value="styles_radio__IqZef")
    #     rate_ten.click()
    #     auth_title = self.driver.find_element(by=By.CLASS_NAME, value="passp-add-account-page-title")
    #     assert auth_title.text.__contains__("Войдите или зарегистрируйтесь")

    def tearDown(self):
        self.driver.close()


if __name__ == "__main__":
    unittest.main()
